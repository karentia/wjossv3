@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>


               
                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                    <p>BACA PETUNJUK SEBELUM INPUT DATA ANDA</p>
                        <div class="form-group row">
                            <label for="Name" class="col-md-4 col-form-label text-md-right">{{ __('Nama Lengkap') }}</label>

                            <div class="col-md-6">
                                <input id="Name" type="text" class="form-control{{ $errors->has('Name') ? ' is-invalid' : '' }}" name="Name" value="{{ old('Name') }}" required autofocus>

                                @if ($errors->has('Name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('Name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Phone') }}</label>

                            <div class="col-md-6">
                                <input id="Phone" type="number" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="Phone" value="{{ old('Phone') }}" required>

                                @if ($errors->has('Phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('Phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>

                            <div class="col-md-6">
                                <input id="Email" type="email" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="Email" value="{{ old('Email') }}" required autofocus>

                                @if ($errors->has('Email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('Email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                         <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Username') }}</label>

                            <div class="col-md-6">
                                <input id="Username" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="Username" value="{{ old('Username') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('Username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                          <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="Password" type="Password" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="Password" value="{{ old('Password') }}" required autofocus>

                                @if ($errors->has('Password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('Password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Rekomendator') }}</label>

                            <div class="col-md-6">
                                <input id="Rekomendator" type="number" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="Rekomendator" value="{{ old('Rekomendator') }}" required autofocus>

                                @if ($errors->has('Rekomendator'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('Rekomendator') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset') }}
                                    
                                </button>

                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>

                                </div>
                        </div>


                  <!--      <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div> -->

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

