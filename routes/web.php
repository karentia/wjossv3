<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

get: tanpa parameter
post: using db
note button submit add post
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('hello', function(){
    return view('hello');
});

// Route::get('games', function () {
//     $data['games'] = ['Balloon Fight', 'Donkey Kong', 'Excitebike'];
//     return view('games')->with('games', $games);
// });


// Route::get('games', function () {
    
//     $data['games'] = ['Gradius', 'Kirbys Adventure', 'Pac-Man'];
//     $data['publishers'] = ['Konami', 'Nintendo', 'Bandai Namco'];
    
//     return view('games', $data);
// });

Route::get('games', function () {
    
    $data['games'] = ['Gradius', 'Kirbys Adventure', 'Pac-Man'];
    $data['publishers'] = ['Konami', 'Nintendo', 'Bandai Namco'];
    $data['releasedates'] = ['1986', '1993', '1984'];
    
    return view('games', $data);
});

Route::get('/register', 'RegistrationController@create');
Route::get('/helloworld', 'HelloWorldController@index');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
